---
layout: default
title: help
---

### Getting help with BCE

If you encountered problems installing or using BCE here are some
options available that may help:

#### Install Sessions

During the install session experts are available to help you install
BCE and answer your questions.

The next BCE install session will be held on:

**None Scheduled at this time; please request help via the [support forum](https://groups.google.com/forum/#!forum/ucb-bce)**

Attend the next scheduled group installation session where experts
will help you install BCE, get you familiar with the environment, or
help you find an alternative if your laptop is not capable of running
BCE.

#### BCE Community Support Forum

Ask for help via email on the [BCE community support forum](https://groups.google.com/forum/#!forum/ucb-bce)


#### Troubleshooting Tips

If you'd like to attempt to fix it yourself, you may want to check out our [Troubleshooting Tips](troubleshooting-tips.html).

Or see our [screencasts](screencasts.html).
